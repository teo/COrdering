{ mkDerivation, base, stdenv }:
mkDerivation {
  pname = "COrdering";
  version = "2.4";
  src = ./.;
  libraryHaskellDepends = [ base ];
  description = "An algebraic data type similar to Prelude Ordering";
  license = stdenv.lib.licenses.bsd3;
}
